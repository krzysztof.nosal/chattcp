/*
 * This file is generated by jOOQ.
 */
package chat_tcp.generated.chatdb;


import chat_tcp.generated.chatdb.tables.Messages;
import chat_tcp.generated.chatdb.tables.Users;
import org.jooq.Catalog;
import org.jooq.Table;
import chat_tcp.generated.DefaultCatalog;
import org.jooq.impl.SchemaImpl;

import java.util.Arrays;
import java.util.List;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class Chattcp extends SchemaImpl {

    /**
     * The reference instance of <code>chattcp</code>
     */
    public static final Chattcp CHATTCP = new Chattcp();
    private static final long serialVersionUID = -1031976124;
    /**
     * The table <code>chattcp.messages</code>.
     */
    public final Messages MESSAGES = Messages.MESSAGES;

    /**
     * The table <code>chattcp.users</code>.
     */
    public final Users USERS = Users.USERS;

    /**
     * No further instances allowed
     */
    private Chattcp() {
        super("", null);
    }


    @Override
    public Catalog getCatalog() {
        return DefaultCatalog.DEFAULT_CATALOG;
    }

    @Override
    public final List<Table<?>> getTables() {
        return Arrays.<Table<?>>asList(
                Messages.MESSAGES,
                Users.USERS);
    }
}
