/*
 * This file is generated by jOOQ.
 */
package chat_tcp.generated.chatdb;


import chat_tcp.generated.chatdb.tables.Messages;
import chat_tcp.generated.chatdb.tables.Users;
import org.jooq.Identity;
import org.jooq.TableField;
import org.jooq.UniqueKey;
import chat_tcp.generated.chatdb.tables.records.MessagesRecord;
import chat_tcp.generated.chatdb.tables.records.UsersRecord;
import org.jooq.impl.Internal;


/**
 * A class modelling foreign key relationships and constraints of tables of
 * the <code>chattcp</code> schema.
 */
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class Keys {

    // -------------------------------------------------------------------------
    // IDENTITY definitions
    // -------------------------------------------------------------------------

    public static final Identity<MessagesRecord, Integer> IDENTITY_MESSAGES = Identities0.IDENTITY_MESSAGES;

    // -------------------------------------------------------------------------
    // UNIQUE and PRIMARY KEY definitions
    // -------------------------------------------------------------------------

    public static final UniqueKey<MessagesRecord> KEY_MESSAGES_PRIMARY = UniqueKeys0.KEY_MESSAGES_PRIMARY;
    public static final UniqueKey<UsersRecord> KEY_USERS_PRIMARY = UniqueKeys0.KEY_USERS_PRIMARY;
    public static final UniqueKey<UsersRecord> KEY_USERS_NAME = UniqueKeys0.KEY_USERS_NAME;

    // -------------------------------------------------------------------------
    // FOREIGN KEY definitions
    // -------------------------------------------------------------------------


    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Identities0 {
        public static Identity<MessagesRecord, Integer> IDENTITY_MESSAGES = Internal.createIdentity(Messages.MESSAGES, Messages.MESSAGES.ID);
    }

    private static class UniqueKeys0 {
        public static final UniqueKey<MessagesRecord> KEY_MESSAGES_PRIMARY = Internal.createUniqueKey(Messages.MESSAGES, "KEY_messages_PRIMARY", new TableField[]{Messages.MESSAGES.ID}, true);
        public static final UniqueKey<UsersRecord> KEY_USERS_PRIMARY = Internal.createUniqueKey(Users.USERS, "KEY_users_PRIMARY", new TableField[]{Users.USERS.NAME}, true);
        public static final UniqueKey<UsersRecord> KEY_USERS_NAME = Internal.createUniqueKey(Users.USERS, "KEY_users_name", new TableField[]{Users.USERS.NAME}, true);
    }
}
