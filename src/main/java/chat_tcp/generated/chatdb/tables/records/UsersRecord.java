/*
 * This file is generated by jOOQ.
 */
package chat_tcp.generated.chatdb.tables.records;


import chat_tcp.generated.chatdb.tables.Users;
import org.jooq.Field;
import org.jooq.Record1;
import org.jooq.Record3;
import org.jooq.Row3;
import org.jooq.impl.UpdatableRecordImpl;


/**
 * This class is generated by jOOQ.
 */
@SuppressWarnings({"all", "unchecked", "rawtypes"})
public class UsersRecord extends UpdatableRecordImpl<UsersRecord> implements Record3<String, Integer, String> {

    private static final long serialVersionUID = 1380990594;

    /**
     * Create a detached UsersRecord
     */
    public UsersRecord() {
        super(Users.USERS);
    }

    /**
     * Create a detached, initialised UsersRecord
     */
    public UsersRecord(String name, Integer adminrights, String password) {
        super(Users.USERS);

        set(0, name);
        set(1, adminrights);
        set(2, password);
    }

    /**
     * Getter for <code>chattcp.users.name</code>.
     */
    public String getName() {
        return (String) get(0);
    }

    /**
     * Setter for <code>chattcp.users.name</code>.
     */
    public void setName(String value) {
        set(0, value);
    }

    /**
     * Getter for <code>chattcp.users.adminrights</code>.
     */
    public Integer getAdminrights() {
        return (Integer) get(1);
    }

    /**
     * Setter for <code>chattcp.users.adminrights</code>.
     */
    public void setAdminrights(Integer value) {
        set(1, value);
    }

    // -------------------------------------------------------------------------
    // Primary key information
    // -------------------------------------------------------------------------

    /**
     * Getter for <code>chattcp.users.password</code>.
     */
    public String getPassword() {
        return (String) get(2);
    }

    // -------------------------------------------------------------------------
    // Record3 type implementation
    // -------------------------------------------------------------------------

    /**
     * Setter for <code>chattcp.users.password</code>.
     */
    public void setPassword(String value) {
        set(2, value);
    }

    @Override
    public Record1<String> key() {
        return (Record1) super.key();
    }

    @Override
    public Row3<String, Integer, String> fieldsRow() {
        return (Row3) super.fieldsRow();
    }

    @Override
    public Row3<String, Integer, String> valuesRow() {
        return (Row3) super.valuesRow();
    }

    @Override
    public Field<String> field1() {
        return Users.USERS.NAME;
    }

    @Override
    public Field<Integer> field2() {
        return Users.USERS.ADMINRIGHTS;
    }

    @Override
    public Field<String> field3() {
        return Users.USERS.PASSWORD;
    }

    @Override
    public String component1() {
        return getName();
    }

    @Override
    public Integer component2() {
        return getAdminrights();
    }

    @Override
    public String component3() {
        return getPassword();
    }

    @Override
    public String value1() {
        return getName();
    }

    @Override
    public Integer value2() {
        return getAdminrights();
    }

    @Override
    public String value3() {
        return getPassword();
    }

    @Override
    public UsersRecord value1(String value) {
        setName(value);
        return this;
    }

    @Override
    public UsersRecord value2(Integer value) {
        setAdminrights(value);
        return this;
    }

    // -------------------------------------------------------------------------
    // Constructors
    // -------------------------------------------------------------------------

    @Override
    public UsersRecord value3(String value) {
        setPassword(value);
        return this;
    }

    @Override
    public UsersRecord values(String value1, Integer value2, String value3) {
        value1(value1);
        value2(value2);
        value3(value3);
        return this;
    }
}
