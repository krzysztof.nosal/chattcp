package chat_tcp.server;

import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;

import java.sql.Connection;
import java.sql.DriverManager;


public class MySqlDbConnection {
    private DSLContext context;
    private Connection connection;

    public MySqlDbConnection(String address, String user, String password) {
        try {

            connection = DriverManager.getConnection("jdbc:sqlite:D:\\__JAVA\\chatTcp\\src\\main\\resources\\db\\chatdb.db");
            this.context = DSL.using(connection, SQLDialect.SQLITE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DSLContext getContext() {
        return context;
    }
}
