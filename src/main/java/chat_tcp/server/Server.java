package chat_tcp.server;

import chat_tcp.client.ClientHandler;
import org.jooq.DSLContext;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicBoolean;

public class Server {

    public static void main(String[] args) throws IOException {
        Vector<ClientHandler> activeConnections = new Vector<>();


        MySqlDbConnection mySqlDbConnection = new MySqlDbConnection("jdbc:mysql://localhost:3306", "root", "");
        DSLContext context = mySqlDbConnection.getContext();

        AtomicBoolean running = new AtomicBoolean(true);


        ServerSocket ss = new ServerSocket(1234);

        Thread chatServer = new Thread(() -> {
            while (running.get()) {

                try {
                    Socket s = ss.accept();


                    System.out.println("New client request received : " + s);

                    DataInputStream inputStream = new DataInputStream(s.getInputStream());
                    DataOutputStream outputStream = new DataOutputStream(s.getOutputStream());
                    System.out.println("Creating a new handler for this client...");
                    System.out.println("remoteSocketAddr: " + s.getRemoteSocketAddress().toString());
                    outputStream.writeUTF("login");
                    ClientHandler clientHandler = new ClientHandler(inputStream, outputStream, context, activeConnections);

                    Thread t = new Thread(clientHandler);

                    System.out.println("Adding this client to active client list");
                    activeConnections.add(clientHandler);

                    t.start();

                } catch (Exception e) {
                    running.set(false);
                    break;

                }

            }

        });
        chatServer.start();


    }
} 