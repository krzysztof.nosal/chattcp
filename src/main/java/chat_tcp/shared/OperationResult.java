package chat_tcp.shared;

public enum OperationResult {
    DONE, NORIGHTS, ERROR, NOSUCHUSER, USEREXISTS, USEROFFLINE, FULLINBOX
}
