package chat_tcp.shared;

import chat_tcp.client.ClientHandler;
import chat_tcp.generated.chatdb.tables.records.MessagesRecord;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import chat_tcp.generated.chatdb.tables.Messages;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Vector;

public class MessageManagement {
    private final DSLContext context;
    private final Vector<ClientHandler> activeUserConnections;
    private final UserManagement userManagement;

    public MessageManagement(DSLContext context, Vector<ClientHandler> activeUserConnections) {
        this.context = context;
        this.activeUserConnections = activeUserConnections;
        this.userManagement = new UserManagement(context);
    }

    public ClientHandler getUserConnection(String name) {
        for (ClientHandler mc : activeUserConnections) {
            if (mc.getName().equals(name)) {
                return mc;
            }
        }
        return null;
    }

    public OperationResult sendMessage(String curUsrName, String curUsrPass, String recipient, String message) {
        if (!userManagement.isPassCorrect(curUsrName, curUsrPass)) {
            return OperationResult.NORIGHTS;
        } else if (!userManagement.userExists(recipient)) {
            return OperationResult.NOSUCHUSER;
        } else if (!isUserAvailable(recipient)) {
            return OperationResult.USEROFFLINE;
        } else if (isInboxFull(recipient)) {
            try {
                getUserConnection(recipient).getOutputStream().writeUTF("you have full inbox and somebody tried to contact you");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return OperationResult.FULLINBOX;
        } else {
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
            Date date = new Date(System.currentTimeMillis());
            MessagesRecord newMessage = context.newRecord(Messages.MESSAGES);
            newMessage.setSender(curUsrName);
            newMessage.setReceiver(recipient);
            newMessage.setTimestamp(formatter.format(date));
            newMessage.setContent(message);
            newMessage.store();
            try {
                getUserConnection(recipient).getOutputStream().writeUTF(formatter.format(date) + ">>>" + curUsrName + ": " + message);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return OperationResult.DONE;
        }
    }

    public boolean isInboxFull(String name) {
        Result<Record> messageResult = context.select()
                .from(Messages.MESSAGES)
                .where(Messages.MESSAGES.RECEIVER.eq(name))
                .fetch();
        if (userManagement.isUserAdmin(name)) {
            return (messageResult.size() > 24);
        } else {
            return (messageResult.size() > 4);
        }
    }

    public boolean isUserAvailable(String name) {
        boolean isAvailable = false;
        for (ClientHandler mc : activeUserConnections) {
            if (mc.getName().equals(name) && mc.isLoggedin()) {
                isAvailable = true;
                break;
            }
        }
        return isAvailable;
    }

    public String getMessagesOfUser(String name) {
        StringBuilder stringBuilder = new StringBuilder();
        Result<Record> messagesResult = context.select()
                .from(Messages.MESSAGES)
                .where(Messages.MESSAGES.RECEIVER.eq(name))
                .fetch();
        messagesResult.forEach(messageFromDb -> {
            stringBuilder.append(messageFromDb.getValue(Messages.MESSAGES.TIMESTAMP));
            stringBuilder.append(">>> ");
            stringBuilder.append(messageFromDb.getValue(Messages.MESSAGES.SENDER));
            stringBuilder.append(" : ");
            stringBuilder.append(messageFromDb.getValue(Messages.MESSAGES.CONTENT));
            stringBuilder.append("\n");
        });
        return stringBuilder.toString();
    }

    public void deleteOldestMessage(String name) {
        Result<Record> messageResult = context.select()
                .from(Messages.MESSAGES)
                .where(Messages.MESSAGES.RECEIVER.eq(name))
                .orderBy(Messages.MESSAGES.ID)
                .fetch();
        Integer id = messageResult.getValue(0, Messages.MESSAGES.ID);
        context.delete(Messages.MESSAGES)
                .where(Messages.MESSAGES.ID.eq(id))
                .execute();
    }
}
