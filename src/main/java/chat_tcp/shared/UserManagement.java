package chat_tcp.shared;

import chat_tcp.generated.chatdb.tables.records.UsersRecord;
import org.jooq.DSLContext;
import org.jooq.Record;
import org.jooq.Result;
import chat_tcp.generated.chatdb.tables.Users;


public class UserManagement {
    private final DSLContext context;

    public UserManagement(DSLContext context) {
        this.context = context;

    }

    public boolean userExists(String name) {
        Result<Record> usersResult = context.select()
                .from(Users.USERS)
                .where(Users.USERS.NAME.eq(name))
                .fetch();
        return usersResult.isNotEmpty();
    }

    public boolean isUserAdmin(String name) {
        Result<Record> userResults = context.select()
                .from(Users.USERS)
                .where(Users.USERS.NAME.eq(name))
                .fetch();
        return userResults.isNotEmpty() && userResults.getValue(0, Users.USERS.ADMINRIGHTS) == 1;
    }

    public boolean isPassCorrect(String name, String pass) {
        Result<Record> userResult = context.select()
                .from(Users.USERS)
                .where(Users.USERS.NAME.eq(name))
                .fetch();
        return userResult.isNotEmpty() && userResult.getValue(0, Users.USERS.PASSWORD).equals(pass);
    }

    public OperationResult addUser(String curUsrName, String curUsrPass, String name, String pass, boolean isAdmin) {
        if (!isPassCorrect(curUsrName, curUsrPass) || !isUserAdmin(curUsrName) || !userExists(curUsrName)) {
            return OperationResult.NORIGHTS;
        } else if (userExists(name)) {
            return OperationResult.USEREXISTS;
        } else {
            UsersRecord newUser = context.newRecord(Users.USERS);
            newUser.setName(name);
            newUser.setPassword(pass);
            newUser.setAdminrights((isAdmin ? 1 : 0));
            newUser.store();
            return OperationResult.DONE;
        }
    }

    public OperationResult deleteUser(String curUsrName, String curUsrPass, String userToDelete) {
        if (!isPassCorrect(curUsrName, curUsrPass) || !isUserAdmin(curUsrName) || !userExists(curUsrName)) {
            return OperationResult.NORIGHTS;
        } else if (!userExists(userToDelete)) {
            return OperationResult.NOSUCHUSER;
        } else {
            context.delete(Users.USERS)
                    .where(Users.USERS.NAME.eq(userToDelete))
                    .execute();
            return OperationResult.DONE;
        }
    }

    public OperationResult changePassword(String name, String newPassword) {
        context.update(Users.USERS)
                .set(Users.USERS.PASSWORD, newPassword)
                .where(Users.USERS.NAME.eq(name))
                .execute();
        return OperationResult.DONE;
    }

    public String printUsers() {
        Result<Record> userResult = context.select()
                .from(Users.USERS)
                .fetch();
        StringBuilder stringBuilder = new StringBuilder();
        userResult.forEach(userFromDb -> {
            stringBuilder.append("name: ");
            stringBuilder.append(userFromDb.getValue(Users.USERS.NAME));
            stringBuilder.append(">>pass: ");
            stringBuilder.append(userFromDb.getValue(Users.USERS.PASSWORD));
            stringBuilder.append(">>admin: ");
            stringBuilder.append(userFromDb.getValue(Users.USERS.ADMINRIGHTS) == 1);
            stringBuilder.append("\n");
        });
        return stringBuilder.toString();
    }

}