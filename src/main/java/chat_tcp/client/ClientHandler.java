package chat_tcp.client;

import chat_tcp.shared.OperationResult;
import chat_tcp.shared.MessageManagement;
import chat_tcp.shared.UserManagement;
import org.jooq.DSLContext;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.Vector;

public class ClientHandler implements Runnable {
    static final String MESSAGE_NO_ADMIN_RIGHS = "you need admin rights to do this";
    static final String DEFAULT_NAME = "defaultName";
    private final DataInputStream inputStream;
    private final DataOutputStream outputStream;
    private final UserManagement userManagement;
    private final MessageManagement messageManagement;
    boolean waitingForPassword = false;
    String pass = "";
    String loginToCheck = "";
    private String name;
    private boolean isloggedin;
    
    public ClientHandler(DataInputStream inputStream, DataOutputStream outputStream, DSLContext context, Vector<ClientHandler> activeConnections) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.isloggedin = false;
        this.userManagement = new UserManagement(context);
        this.messageManagement = new MessageManagement(context, activeConnections);
    }

    public boolean isLoggedin() {
        return isloggedin;
    }

    public void setLoggedin(boolean isloggedin) {
        this.isloggedin = isloggedin;
    }

    public String getName() {
        return name;
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }

    private void logout() {
        try {
            this.outputStream.writeUTF("logging out");
            this.outputStream.writeUTF("enter login");
            this.isloggedin = false;
            this.name = DEFAULT_NAME;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showUsers() {
        try {
            if (userManagement.isUserAdmin(this.name)) {
                outputStream.writeUTF(userManagement.printUsers());
            } else {
                outputStream.writeUTF(MESSAGE_NO_ADMIN_RIGHS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void deleteUser() {
        try {
            if (userManagement.isUserAdmin(this.name)) {
                outputStream.writeUTF("type name of user to delete:");
                String userToDelete = inputStream.readUTF();
                outputStream.writeUTF(userManagement.deleteUser(this.name, this.pass, userToDelete).toString());
            } else {
                outputStream.writeUTF(MESSAGE_NO_ADMIN_RIGHS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void createUser() {
        try {
            if (userManagement.isUserAdmin(this.name)) {
                outputStream.writeUTF("type name of new user:");
                String userToCreate = inputStream.readUTF();
                String passOfNewUser = "";
                outputStream.writeUTF("type new pass:");
                passOfNewUser = inputStream.readUTF();
                outputStream.writeUTF("is user admin (y/n):");
                boolean isNewAdmin = inputStream.readUTF().equals("y");
                outputStream.writeUTF(userManagement.addUser(this.name, this.pass, userToCreate, passOfNewUser, isNewAdmin).toString());
            } else {
                outputStream.writeUTF(MESSAGE_NO_ADMIN_RIGHS);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userCommandHandling(String received) {
        try {
            switch (received) {
                case "logout":
                    logout();
                    break;
                case "refresh":
                    outputStream.writeUTF(messageManagement.getMessagesOfUser(this.name));
                    break;
                case "deleteMsg":
                    messageManagement.deleteOldestMessage(this.name);
                    outputStream.writeUTF("DONE");
                    break;
                case "showAllUsers":
                    showUsers();
                    break;
                case "deleteUser":
                    deleteUser();
                    break;
                case "createUser":
                    createUser();
                    break;
                case "changePswd":
                    outputStream.writeUTF("type new pass:");
                    String newPass = inputStream.readUTF();
                    outputStream.writeUTF(userManagement.changePassword(this.name, newPass).toString());
                    break;
                case "help":
                    StringBuilder stringBuilder = new StringBuilder();
                    if (userManagement.isUserAdmin(this.name)) {
                        stringBuilder.append("refresh\n");
                        stringBuilder.append("deleteMsg\n");
                        stringBuilder.append("changePswd\n");
                        stringBuilder.append("showAllUsers\n");
                        stringBuilder.append("deleteUser\n");
                        stringBuilder.append("createUser\n");
                        stringBuilder.append("changeUsrPass\n");
                        outputStream.writeUTF(stringBuilder.toString());
                    } else {
                        stringBuilder.append("refresh\n");
                        stringBuilder.append("deleteMsg\n");
                        stringBuilder.append("changePswd\n");
                        outputStream.writeUTF(stringBuilder.toString());
                    }
                    break;
                case "!!quit":
                    break;
                default:
                    if (received.contains("#")) {
                        StringTokenizer st = new StringTokenizer(received, "#");
                        String messageToSend = st.nextToken();
                        String recipient = st.nextToken();
                        OperationResult result = messageManagement.sendMessage(this.name, this.pass, recipient, messageToSend);
                        if (result != OperationResult.DONE) {
                            outputStream.writeUTF(result.toString());
                        }
                    } else {
                        outputStream.writeUTF("no such command");
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void userLoginSequence(String received) {
        try {
            if (waitingForPassword) {
                pass = received;
                if (received.length() > 0) {
                    if (userManagement.isPassCorrect(loginToCheck, received)) {
                        outputStream.writeUTF("loged in sucessfully");
                        this.name = loginToCheck;
                        isloggedin = true;
                        waitingForPassword = false;

                    } else {
                        outputStream.writeUTF("incorrect password");
                        outputStream.writeUTF("login");
                        waitingForPassword = false;
                    }
                }
            } else {
                if (userManagement.userExists(received)) {
                    outputStream.writeUTF("enter password");
                    loginToCheck = received;
                    waitingForPassword = true;
                } else {
                    outputStream.writeUTF("user does not exist");
                    outputStream.writeUTF("login");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        String received = "";
        while (!received.equals("!!quit")) {
            try {
                received = inputStream.readUTF();
                if (received.length() > 255) {
                    outputStream.writeUTF("to long message");
                    received = "";
                }
                if (isloggedin) {
                    userCommandHandling(received);
                } else {
                    userLoginSequence(received);

                }

            } catch (IOException e) {
                try {
                    this.inputStream.close();
                    this.outputStream.close();

                } catch (IOException ee) {
                    ee.printStackTrace();
                }
            }

        }

    }
}

