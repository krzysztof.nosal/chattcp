package chat_tcp.client;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class MainClient1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter ip address:");
        String ipAdress = scanner.nextLine();
        System.out.println("enter port:");
        int tcpPort = scanner.nextInt();
        ClientConnectionHandler connectionHandler = new ClientConnectionHandler();
        while (!connectionHandler.connect(tcpPort, ipAdress)) {
            System.out.println("trying to connect");
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        new Client(connectionHandler.getInputStream(), connectionHandler.getOutputStream(), scanner);
    }
}
