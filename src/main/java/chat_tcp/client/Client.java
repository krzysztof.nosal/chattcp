package chat_tcp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Scanner;

public class Client {
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;
    private Scanner scanner = null;
    private boolean closing = false;
    Thread sendMessage = new Thread(() -> {
        while (!closing) {
            String msg = scanner.nextLine();
            try {
                if (!msg.equals(" ") && !msg.equals("")) {
                    outputStream.writeUTF(msg);
                    if (msg.equals("!!quit")) {
                        System.out.println("Program ended");
                        closing = true;
                    }
                }
            } catch (IOException e) {
                System.out.println("Server Error. closing app");
                closing = true;
                break;
            }
        }
    });
    // readMessage thread
    Thread readMessage = new Thread(() -> {
        while (!closing) {
            try {
                String msg = inputStream.readUTF();
                System.out.println(msg);
            } catch (IOException e) {
                System.out.println("Server Error. closing app");
                closing = true;
                break;
            }

        }
    });

    public Client(DataInputStream inputStream, DataOutputStream outputStream, Scanner scanner) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
        this.scanner = scanner;
        sendMessage.start();
        readMessage.start();
    }


}



