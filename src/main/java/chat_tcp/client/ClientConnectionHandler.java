package chat_tcp.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;

public class ClientConnectionHandler {
    Socket s = null;
    private int portNumber;
    private String ipAddress;
    private DataInputStream inputStream = null;
    private DataOutputStream outputStream = null;

    public ClientConnectionHandler() {
        try {
            s = new Socket();
            inputStream = new DataInputStream(s.getInputStream());
            outputStream = new DataOutputStream(s.getOutputStream());
        } catch (Exception e) {
        }
    }

    public boolean connect(int portNumber, String ipAddress) {

        InetSocketAddress ip = new InetSocketAddress(ipAddress, portNumber);
        try {
            s.connect(ip);
            inputStream = new DataInputStream(s.getInputStream());
            outputStream = new DataOutputStream(s.getOutputStream());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public DataInputStream getInputStream() {
        return inputStream;
    }

    public DataOutputStream getOutputStream() {
        return outputStream;
    }
}
